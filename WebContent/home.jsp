<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<link rel="stylesheet" href="./css/main.css">
		<title>	ホーム画面</title>
	</head>
	<body>
		<header>
			<nav id="global-navi">
				<ul>
					<li><a href="message">新規投稿</a></li>

					<c:if test="${ (loginUser.branchId == 1) && (loginUser.departmentId == 1) }">
						<li><a href="management">ユーザー管理</a></li>
					</c:if>

					<li><a href="logout">ログアウト</a></li>
				</ul>
			</nav>
		</header>

		<section id="main">
			<section id="narrow-form">
				<form action="./" method="get">
					<div id="date-narrow">
						<label>表示期間</label>
						<input type="date" name="start" value="${ start }">
						~
						<input type="date" name="end" value="${ end }">
					</div>

					<div id="category-narrow">
						<label for="category">カテゴリ</label>
						<input type="text" name="category" value="${ category }" id="category">
					</div>

					<div id="narrow-buttom">
						<input type="submit" value="絞り込み">
					</div>
				</form>
			</section>

			<c:if test="${ not empty errorMessages }">
				<section id="error">
					<ul>
						<c:forEach items="${ errorMessages }" var="errorMessage">
							<li><c:out value="${ errorMessage }" /></li>
						</c:forEach>
						<c:remove var="errorMessages" scope="session" />
					</ul>
				</section>
			</c:if>

			<section id="contents">
				<c:forEach items="${ messages }" var="message">
					<div id="message-view">
						<div id="message">
							<p class="user-name">@<c:out value="${ message.name }" /></p>
							<p><c:out value="${ message.title }" /></p>
							<pre><c:out value="${ message.text }" /></pre>
							<p>#<c:out value="${ message.category }" /></p>
							<p class="date"><fmt:formatDate value="${ message.createdDate }" pattern="yyyy/MM/dd HH:mm:ss" /></p>

							<c:if test="${ message.userId == loginUser.id }">
								<form action="deleteMessage" method="post" name="deleteMessageForm" id="delete">
									<button name="deleteMessageId" value="${ message.messageId }" onclick="return deleteMessage()">削除</button>
								</form>
							</c:if>
						</div>

						<div>
							<form action="comment" method="post">
								<textarea cols="15" rows="2" name="text"></textarea>
								<button type="submit" name="messageId" value="${ message.messageId }">コメント</button>
							</form>
						</div>

						<div id="comments">
							<ul>
								<c:forEach items="${ comments }" var="comment">
									<c:if test="${ comment.messageId == message.messageId }">
										<li id="comment">
											<p class="user-name">@<c:out value="${ comment.name }" /></p>
											<pre><c:out value="${ comment.text }" /></pre>
											<p class="date"><fmt:formatDate value="${ comment.createdDate }" pattern="yyyy/MM/dd HH:mm:ss" /></p>

											<c:if test="${ comment.userId == loginUser.id }">
												<form action="deleteComment" method="post" name="deleteCommentForm" id="delete">
													<button name="deleteCommentId" value="${ comment.commentId }" onclick="return deleteComment()">削除</button>
												</form>
											</c:if>
										</li>
									</c:if>
								</c:forEach>
							</ul>
						</div>
					</div>

					<hr>
				</c:forEach>
			</section>
		</section>

		<section id="copy">
			<small>Copyright(c) Endo Yu</small>
		</section>

		<script src="./js/main.js"></script>
	</body>
</html>