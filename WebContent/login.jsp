<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<link rel="stylesheet" href="./css/main.css">
		<title>ログイン画面</title>
	</head>
	<body>
		<section id="main">
			<c:if test="${ not empty errorMessages }">
				<section id="error">
					<ul>
						<c:forEach items="${ errorMessages }" var="errorMessage">
							<li><c:out value="${ errorMessage }" /></li>
						</c:forEach>
						<c:remove var="errorMessages" scope="session" />
					</ul>
				</section>
			</c:if>

			<section id="contents">
				<form action="login" method="post">
					<div class="form-content">
						<label for="account">アカウント</label>
						<input type="text" id="account" name="account" value="${ account }">
					</div>

					<div class="form-content">
						<label for="password">パスワード</label>
						<input type="password" id="password" name="password">
					</div>

					<div class="form-content">
						<input type="submit" value="ログイン">
					</div>
				</form>
			</section>
		</section>

		<section id="copy">
			<small>Copyright(c) Endo Yu</small>
		</section>

	</body>
</html>