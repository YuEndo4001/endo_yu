<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<link rel="stylesheet" href="./css/main.css">
		<title>新規投稿画面</title>
	</head>
	<body>
		<header>
			<nav id="global-navi">
				<ul>
					<li><a href="./">ホーム</a></li>
				</ul>
			</nav>
		</header>

		<section id="main">
			<c:if test="${ not empty errorMessages }">
				<section id="error">
					<ul>
						<c:forEach items="${ errorMessages }" var="errorMessage">
							<li><c:out value="${ errorMessage }" /></li>
						</c:forEach>
						<c:remove var="errorMessages" scope="session" />
					</ul>
				</section>
			</c:if>

			<section id="contents">
				<form action="message" method="post" id="message-form">
					<div>
						<label for="title">件名</label>
						<input type="text" id="title" name="title" value="${ message.title }">
					</div>

					<div>
						<label for="category">カテゴリ</label>
						<input type="text" id="category" name="category" value="${ message.category }">
					</div>

					<div>
						<label for="text">本文</label>
						<textarea cols="30" rows="4" id="text" name="text"><c:out value="${ message.text }" /></textarea>
					</div>

					<div>
						<input type="submit" value="投稿">
					</div>
				</form>
			</section>
		</section>

		<section id="copy">
			<small>Copyright(c) Endo Yu</small>
		</section>

	</body>
</html>