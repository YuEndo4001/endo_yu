const deleteMessage = () => {

	let result = window.confirm("削除してもよろしいですか？");

	if (result) {

		document.deleteMessageForm.submit();

	} else {

		return false;

	}
}

const deleteComment = () => {

	let result = window.confirm("削除してもよろしいですか？");

	if (result) {

		document.deleteCommentForm.submit();

	} else {

		return false;

	}
}

const changeRevival = () => {

	let result = window.confirm("復活させてもよろしいですか？");

	if (result) {

		document.isStoppedForm.submit();

	} else {

		return false;

	}
}

const changeStopped = () => {

	let result = window.confirm("停止させてもよろしいですか？");

	if (result) {

		document.isStoppedForm.submit();

	} else {

		return false;

	}
}
