<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<link rel="stylesheet" href="./css/main.css">
		<title>ユーザー新規登録</title>
	</head>
	<body>
		<header>
			<nav id="global-navi">
				<ul>
					<li>
						<a href="management">ユーザー管理</a>
					</li>
				</ul>
			</nav>
		</header>

		<section id="main">
			<c:if test="${ not empty errorMessages }">
				<section id="error">
					<ul>
						<c:forEach items="${ errorMessages }" var="errorMessage">
							<li><c:out value="${ errorMessage }" /></li>
						</c:forEach>
						<c:remove var="errorMessages" scope="session" />
					</ul>
				</section>
			</c:if>

			<section id="contents">
				<form action="signup" method="post" id="signup-form">
					<div>
						<label for="account">アカウント</label>
						<input type="text" id="account" name="account" value="${ user.account }">
					</div>

					<div>
						<label for="password">パスワード</label>
						<input type="password" id="password" name="password">
					</div>

					<div>
						<label for="confirmation">確認用パスワード</label>
						<input type="password" id="confirmation" name="confirmation">
					</div>

					<div>
						<label for="name">ユーザー名</label>
						<input type="text" id="name" name="name" value="${ user.name }">
					</div>

					<div>
						<label>支社</label>
						<select name="branchId">
							<c:forEach items="${ branches }" var="branch">
								<option value="${branch.id }" <c:if test="${ user.branchId == branch.id }"><c:out value="selected" /></c:if>>
									<c:out value="${ branch.name }" />
								</option>
							</c:forEach>
						</select>
					</div>

					<div>
						<label>部署</label>
						<select name="departmentId">
							<c:forEach items="${ departments }" var="department">
								<option value="${ department.id }" <c:if test="${ user.departmentId == department.id }"><c:out value="selected" /></c:if>>
									<c:out value="${ department.name }" />
								</option>
							</c:forEach>
						</select>
					</div>

					<div>
						<input type="submit" value="登録">
					</div>
				</form>
			</section>
		</section>

		<section id="copy">
			<small>Copyright(c) Endo Yu</small>
		</section>

	</body>
</html>