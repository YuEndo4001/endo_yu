<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<link rel="stylesheet" href="./css/main.css">
		<title>ユーザー管理画面</title>
	</head>
	<body>
		<header>
			<nav id="global-navi">
				<ul>
					<li><a href="./">ホーム</a></li>
					<li><a href="signup">ユーザー新規登録</a></li>
				</ul>
			</nav>
		</header>

		<section id="main">
			<c:if test="${ not empty errorMessages }">
				<section id="error">
					<ul>
						<c:forEach items="${ errorMessages }" var="errorMessage">
							<li><c:out value="${ errorMessage }" /></li>
						</c:forEach>
						<c:remove var="errorMessages" scope="session" />
					</ul>
				</section>
			</c:if>

			<section id="contents">
				<table id="users">
					<tr>
						<th>アカウント</th>
						<th>ユーザー名</th>
						<th>支社</th>
						<th>部署</th>
						<th>状態</th>
						<th></th>
					<tr>

					<c:forEach items="${ users }" var="user">
						<tr id="user">
							<td><c:out value="${ user.account }" /></td>
							<td><c:out value="${ user.name }" /></td>
							<td><c:out value="${ user.branch }" /></td>
							<td><c:out value="${ user.department }" /></td>

							<c:choose>
								<c:when test="${ user.isStopped == 0 }">
									<td><c:out value="稼働中" /></td>
								</c:when>
								<c:when test="${ user.isStopped == 1 }">
									<td><c:out value="停止中" /></td>
								</c:when>
							</c:choose>

							<td>
								<form action="setting" method="get">
									<button type="submit" name="settingUser" value="${ user.id }">編集</button>
								</form>

								<c:if test="${ user.id != loginUser.id }">
									<form action="stop" method="post" name="isStoppedForm">
										<input type="hidden" name="userId" value="${ user.id }">

										<c:choose>
											<c:when test="${ user.isStopped == 1 }">
												<button type="submit" name="isStopped" value="0" onclick="return changeRevival()">復活</button>
											</c:when>
											<c:when test="${ user.isStopped == 0 }">
												<button type="submit" name="isStopped" value="1" onclick="return changeStopped()">停止</button>
											</c:when>
										</c:choose>
									</form>
								</c:if>
							</td>
						</tr>
					</c:forEach>
				</table>
			</section>
		</section>

		<section id="copy">
			<small>Copyright(c) Endo Yu</small>
		</section>

		<script src="./js/main.js"></script>
	</body>
</html>