package beans;

import java.util.Date;

public class UserComment {

	private String name;
	private String text;
	private int userId;
	private int messageId;
	private int commentId;
	private Date createdDate;

	public void setName(String name) {
		this.name = name;
	}
	public String getName() {
		return this.name;
	}

	public void setText(String text) {
		this.text = text;
	}
	public String getText() {
		return this.text;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}
	public int getUserId( ) {
		return this.userId;
	}

	public void setMessageId(int messageId) {
		this.messageId = messageId;
	}
	public int getMessageId() {
		return this.messageId;
	}

	public void setCommentId(int commentId) {
		this.commentId = commentId;
	}
	public int getCommentId() {
		return this.commentId;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public Date getCreatedDate() {
		return this.createdDate;
	}
}
