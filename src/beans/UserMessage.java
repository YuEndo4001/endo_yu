package beans;

import java.util.Date;

public class UserMessage {

	private String name;
	private String title;
	private String category;
	private String text;
	private int userId;
	private int messageId;
	private Date createdDate;

	public void setName(String name) {
		this.name = name;
	}
	public String getName() {
		return this.name;
	}

	public void setTitle(String title) {
		this.title = title;
	}
	public String getTitle() {
		return this.title;
	}

	public void setCategory(String category) {
		this.category = category;
	}
	public String getCategory() {
		return this.category;
	}

	public void setText(String text) {
		this.text = text;
	}
	public String getText() {
		return this.text;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}
	public int getUserId() {
		return this.userId;
	}

	public void setMessageId(int messageId) {
		this.messageId = messageId;
	}
	public int getMessageId() {
		return this.messageId;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public Date getCreatedDate() {
		return this.createdDate;
	}
}
