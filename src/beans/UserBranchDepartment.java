package beans;

public class UserBranchDepartment {

	private int id;
	private String account;
	private String name;
	private String branch;
	private String department;
	private byte isStopped;

	public void setId(int id) {
		this.id = id;
	}
	public int getId() {
		return this.id;
	}

	public void setAccount(String account) {
		this.account = account;
	}
	public String getAccount() {
		return this.account;
	}

	public void setName(String name) {
		this.name = name;
	}
	public String getName() {
		return this.name;
	}

	public void setBranch(String branch) {
		this.branch = branch;
	}
	public String getBranch() {
		return this.branch;
	}

	public void setDepartment(String department) {
		this.department = department;
	}
	public String getDepartment() {
		return this.department;
	}

	public void setIsStopped(byte isStopped) {
		this.isStopped = isStopped;
	}
	public byte getIsStopped() {
		return this.isStopped;
	}
}
