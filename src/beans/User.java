package beans;

import java.io.Serializable;
import java.util.Date;

public class User implements Serializable {

	public static final int RESUMED = 0;
	public static final int STOPPED = 1;

	private int id;
	private String account;
	private String password;
	private String name;
	private int branchId;
	private int departmentId;
	private byte isStopped;
	private Date createdDate;
	private Date updatedDate;

	public void setId(int id) {
		this.id = id;
	}
	public int getId() {
		return this.id;
	}

	public void setAccount(String account) {
		this.account = account;
	}
	public String getAccount() {
		return this.account;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	public String getPassword() {
		return this.password;
	}

	public void setName(String name) {
		this.name = name;
	}
	public String getName() {
		return this.name;
	}

	public void setBranchId(int branchId) {
		this.branchId = branchId;
	}
	public int getBranchId() {
		return this.branchId;
	}

	public void setDepartmentId(int departmentId) {
		this.departmentId = departmentId;
	}
	public int getDepartmentId() {
		return this.departmentId;
	}

	public void setIsStopped(byte isStopped) {
		this.isStopped = isStopped;
	}
	public byte getIsStopped() {
		return this.isStopped;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public Date getCreatedDate() {
		return this.createdDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
	public Date getUpdatedDate() {
		return this.updatedDate;
	}
}
