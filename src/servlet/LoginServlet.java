package servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.User;
import service.UserService;

@WebServlet(urlPatterns = { "/login" })
public class LoginServlet extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.getRequestDispatcher("login.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String account = request.getParameter("account");
		String password = request.getParameter("password");
		List<String> errorMessages = new ArrayList<>();

		if (!isValid(account, password, errorMessages)) {
			request.setAttribute("errorMessages", errorMessages);
			request.getRequestDispatcher("login.jsp").forward(request, response);
			return;
		}

		User user = new UserService().select(account, password);

		if (!isValid(user, errorMessages)) {
			request.setAttribute("account", account);
			request.setAttribute("errorMessages", errorMessages);
			request.getRequestDispatcher("login.jsp").forward(request, response);
			return;
		}

		HttpSession session = request.getSession();

		session.setAttribute("loginUser", user);
		response.sendRedirect("./");
	}

	private boolean isValid(String account, String password, List<String> errorMessages) {
		if (StringUtils.isBlank(account)) {
			errorMessages.add("アカウントを入力してください。");
		}

		if (StringUtils.isBlank(password)) {
			errorMessages.add("パスワードを入力してください。");
		}

		if (!errorMessages.isEmpty()) {
			return false;
		}

		return true;
	}

	private boolean isValid(User user, List<String> errorMessages) {
		if ((user == null) || (user.getIsStopped() == User.STOPPED)) {
			errorMessages.add("アカウントまたはパスワードが誤っています。");
		}

		if (!errorMessages.isEmpty()) {
			return false;
		}

		return true;
	}
}
