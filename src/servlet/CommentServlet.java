package servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.Comment;
import beans.User;
import service.CommentService;

@WebServlet(urlPatterns = { "/comment" })
public class CommentServlet extends HttpServlet {

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		Comment comment = getComment(request);
		List<String> errorMessages = new ArrayList<>();
		HttpSession session = request.getSession();

		if (!isValid(comment, errorMessages)) {
			session.setAttribute("errorMessages", errorMessages);
			response.sendRedirect("./");
			return;
		}

		User loginUser = (User) request.getSession().getAttribute("loginUser");

		comment.setUserId(loginUser.getId());
		new CommentService().insert(comment);
		response.sendRedirect("./");
	}

	private Comment getComment(HttpServletRequest request) throws ServletException, IOException {
		Comment comment = new Comment();

		comment.setText(request.getParameter("text"));
		comment.setMessageId(Integer.parseInt(request.getParameter("messageId")));
		return comment;
	}

	private boolean isValid(Comment comment, List<String> errorMessages) {
		String text = comment.getText();

		if (StringUtils.isBlank(text)) {
			errorMessages.add("コメントを入力してください。");
		} else if (text.length() > 500) {
			errorMessages.add("コメントは500文字以下で入力してください。");
		}

		if (!errorMessages.isEmpty()) {
			return false;
		}

		return true;
	}
}
