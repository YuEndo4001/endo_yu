package servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;

import beans.Branch;
import beans.Department;
import beans.User;
import service.BranchService;
import service.DepartmentService;
import service.UserService;

@WebServlet(urlPatterns = { "/setting" })
public class SettingServlet extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String userId = request.getParameter("settingUser");
		List<String> errorMessages = new ArrayList<>();

		if (!userId.matches("^[0-9]+$")) {
			errorMessages.add("不正なパラメータが入力されました。");
			request.getSession().setAttribute("errorMessages", errorMessages);
			response.sendRedirect("./management");
			return;
		}

		User user = new UserService().select(Integer.parseInt(userId));

		if (user == null) {
			errorMessages.add("不正なパラメータが入力されました。");
			request.getSession().setAttribute("errorMessages", errorMessages);
			response.sendRedirect("./management");
			return;
		}

		List<Branch> branches = new BranchService().select();
		List<Department> departments = new DepartmentService().select();

		request.setAttribute("user", user);
		request.setAttribute("branches", branches);
		request.setAttribute("departments", departments);
		request.getRequestDispatcher("setting.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		User user = getUser(request);
		User createdUser = new UserService().select(user.getAccount());
		List<String> errorMessages = new ArrayList<>();

		if (!isValid(request, user, createdUser, errorMessages)) {
			List<Branch> branches = new BranchService().select();
			List<Department> departments = new DepartmentService().select();

			request.setAttribute("user", user);
			request.setAttribute("errorMessages", errorMessages);
			request.setAttribute("branches", branches);
			request.setAttribute("departments", departments);
			request.getRequestDispatcher("setting.jsp").forward(request, response);
			return;
		}

		new UserService().update(user);
		response.sendRedirect("./management");
	}

	private User getUser(HttpServletRequest request) throws ServletException, IOException {
		User user = new User();

		user.setId(Integer.parseInt(request.getParameter("id")));
		user.setAccount(request.getParameter("account"));
		user.setPassword(request.getParameter("password"));
		user.setName(request.getParameter("name"));
		user.setBranchId(Integer.parseInt(request.getParameter("branchId")));
		user.setDepartmentId(Integer.parseInt(request.getParameter("departmentId")));

		return user;
	}

	private boolean isValid(HttpServletRequest request, User user, User createdUser, List<String> errorMessages)
			throws ServletException, IOException {
		String account = user.getAccount();
		String password = user.getPassword();
		String confirmation = request.getParameter("confirmation");
		String name = user.getName();
		int branchId = user.getBranchId();
		int departmentId = user.getDepartmentId();

		if ((createdUser != null) && (user.getId() != createdUser.getId())) {
			errorMessages.add("アカウントが重複しています。");
		}

		if (StringUtils.isBlank(account)) {
			errorMessages.add("アカウントを入力してください。");
		} else if (account.length() < 6) {
			errorMessages.add("アカウントは6文字以上で入力してください。");
		} else if (account.length() > 20) {
			errorMessages.add("アカウントは20文字以下で入力してください。");
		}

		if (!StringUtils.isBlank(password) && (password.length() < 6)) {
			errorMessages.add("パスワードは6文字以上で入力してください。");
		} else if (password.length() > 20) {
			errorMessages.add("パスワードは20文字以下で入力してください。");
		} else if (!password.equals(confirmation)) {
			errorMessages.add("入力したパスワードと確認用パスワードが一致しません。");
		}

		if (StringUtils.isBlank(name)) {
			errorMessages.add("ユーザー名を入力してください。");
		} else if (name.length() > 10) {
			errorMessages.add("ユーザー名は10文字以下で入力してください。");
		}

		if (branchId == 1) {
			if (departmentId > 2) {
				errorMessages.add("支社と部署の組み合わせが不正です。");
			}
		} else if (branchId > 1) {
			if (departmentId <= 2) {
				errorMessages.add("支社と部署の組み合わせが不正です。");
			}
		}

		if (!errorMessages.isEmpty()) {
			return false;
		}

		return true;
	}
}
