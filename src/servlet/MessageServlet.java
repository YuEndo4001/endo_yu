package servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;

import beans.Message;
import beans.User;
import service.MessageService;

@WebServlet(urlPatterns = { "/message"} )
public class MessageServlet extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.getRequestDispatcher("message.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		Message message = getMessage(request);
		List<String> errorMessages = new ArrayList<>();

		if (!isValid(message, errorMessages)) {
			request.setAttribute("message", message);
			request.setAttribute("errorMessages", errorMessages);
			request.getRequestDispatcher("message.jsp").forward(request, response);
			return;
		}

		User loginUser = (User) request.getSession().getAttribute("loginUser");

		message.setUserId(loginUser.getId());
		new MessageService().insert(message);
		response.sendRedirect("./");
	}

	private Message getMessage(HttpServletRequest request) throws ServletException, IOException {
		Message message = new Message();

		message.setTitle(request.getParameter("title"));
		message.setCategory(request.getParameter("category"));
		message.setText(request.getParameter("text"));

		return message;
	}

	private boolean isValid(Message message, List<String> errorMessages) {
		String title = message.getTitle();
		String category = message.getCategory();
		String text = message.getText();

		if (StringUtils.isBlank(title)) {
			errorMessages.add("件名を入力してください。");
		} else if (title.length() > 30) {
			errorMessages.add("件名は30文字以下で入力してください。");
		}

		if (StringUtils.isBlank(category)) {
			errorMessages.add("カテゴリを入力してください。");
		} else if (category.length() > 10) {
			errorMessages.add("カテゴリは10文字以下で入力してください。");
		}

		if (StringUtils.isBlank(text)) {
			errorMessages.add("本文を入力してください。");
		} else if (text.length() > 1000) {
			errorMessages.add("本文は1000文字以下で入力してください。");
		}

		if (!errorMessages.isEmpty()) {
			return false;
		}

		return true;
	}
}
