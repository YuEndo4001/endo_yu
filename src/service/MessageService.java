package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import beans.Message;
import beans.UserMessage;
import dao.MessageDao;
import dao.UserMessageDao;

public class MessageService {

	public void insert(Message message) throws RuntimeException, Error {
		Connection connection = null;

		try {
			connection = getConnection();

			new MessageDao().insert(connection, message);
			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		}  catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	public List<UserMessage> select(String start, String end, String category) throws RuntimeException, Error {
		Connection connection = null;

		if (!StringUtils.isBlank(start)) {
			start += " 00:00:00";
		} else {
			start = "2020-01-01 00:00:00";
		}

		if (!StringUtils.isBlank(end)) {
			end += " 23:59:59";
		} else {
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd kk:mm:ss");
			end = format.format(new Date());
		}

		if (!StringUtils.isBlank(category)) {
			category = "%" + category + "%";
		} else {
			category = "%";
		}

		try {
			connection = getConnection();
			List<UserMessage> messages = new UserMessageDao().select(connection, start, end, category);

			commit(connection);
			return messages;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	public void delete(int messageId) throws RuntimeException, Error {
		Connection connection = null;

		try {
			connection = getConnection();

			new MessageDao().delete(connection, messageId);
			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
}
