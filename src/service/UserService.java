package service;

import static utils.CipherUtil.*;
import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import beans.User;
import beans.UserBranchDepartment;
import dao.UserBranchDepartmentDao;
import dao.UserDao;

public class UserService {

	public void insert(User user) throws RuntimeException, Error {
		Connection connection = null;

		user.setPassword(encrypt(user.getPassword()));

		try {
			connection = getConnection();

			new UserDao().insert(connection, user);
			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	public User select(String account) throws RuntimeException, Error {
		Connection connection = null;

		try {
			connection = getConnection();
			User user = new UserDao().select(connection, account);

			commit(connection);
			return user;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	public User select(String account, String password) throws RuntimeException, Error {
		Connection connection = null;
		String encPassword = encrypt(password);

		try {
			connection = getConnection();
			User user = new UserDao().select(connection, account, encPassword);

			commit(connection);
			return user;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	public List<UserBranchDepartment> select() throws RuntimeException, Error {
		Connection connection = null;

		try {
			connection = getConnection();
			List<UserBranchDepartment> users = new UserBranchDepartmentDao().select(connection);

			commit(connection);
			return users;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	public User select(int id) throws RuntimeException, Error {
		Connection connection = null;

		try {
			connection = getConnection();
			User user = new UserDao().select(connection, id);

			commit(connection);
			return user;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	public void update(User user) throws RuntimeException, Error {
		Connection connection = null;

		if (!StringUtils.isBlank(user.getPassword())) {
			user.setPassword(encrypt(user.getPassword()));
		}

		try {
			connection = getConnection();

			new UserDao().update(connection, user);
			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	public void update(int userId, int isStopped) throws RuntimeException, Error {
		Connection connection = null;

		try {
			connection = getConnection();

			new UserDao().update(connection, userId, isStopped);
			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
}
