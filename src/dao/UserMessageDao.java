package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import beans.UserMessage;
import exception.SQLRuntimeException;

public class UserMessageDao {

	public List<UserMessage> select(Connection connection, String start, String end, String category)
			throws SQLRuntimeException {
		PreparedStatement ps = null;
		StringBuilder sql = new StringBuilder();

		sql.append("select ");
		sql.append("  users.name as name, ");
		sql.append("  messages.title as title, ");
		sql.append("  messages.category as category, ");
		sql.append("  messages.text as text, ");
		sql.append("  messages.user_id as user_id, ");
		sql.append("  messages.id as message_id, ");
		sql.append("  messages.created_date as created_date ");
		sql.append("from users ");
		sql.append("inner join messages ");
		sql.append("on ");
		sql.append("  users.id = user_id and ");
		sql.append("  messages.category like ? and ");
		sql.append("  messages.created_date between ? and ? ");
		sql.append("order by created_date desc;");

		try {
			ps = connection.prepareStatement(sql.toString());

			ps.setString(1, category);
			ps.setString(2, start);
			ps.setString(3, end);

			ResultSet rs = ps.executeQuery();
			List<UserMessage> messages = toMessages(rs);

			return messages;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private List<UserMessage> toMessages(ResultSet rs) throws SQLException {
		List<UserMessage> messages = new ArrayList<>();

		try {
			while (rs.next()) {
				UserMessage message = new UserMessage();

				message.setName(rs.getString("name"));
				message.setTitle(rs.getString("title"));
				message.setCategory(rs.getString("category"));
				message.setText(rs.getString("text"));
				message.setUserId(rs.getInt("user_id"));
				message.setMessageId(rs.getInt("message_id"));
				message.setCreatedDate(rs.getTimestamp("created_date"));

				messages.add(message);
			}

			return messages;
		} finally {
			close(rs);
		}
	}
}
