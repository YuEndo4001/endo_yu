package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import beans.Comment;
import exception.SQLRuntimeException;

public class CommentDao {

	public void insert(Connection connection, Comment comment) throws SQLRuntimeException {
		PreparedStatement ps = null;
		StringBuilder sql = new StringBuilder();

		sql.append("insert into comments ( ");
		sql.append("  text, ");
		sql.append("  user_id, ");
		sql.append("  message_id, ");
		sql.append("  created_date, ");
		sql.append("  updated_date ");
		sql.append(") values ( ");
		sql.append("  ?, ");
		sql.append("  ?, ");
		sql.append("  ?, ");
		sql.append("  current_timestamp, ");
		sql.append("  current_timestamp);");

		try {
			ps = connection.prepareStatement(sql.toString());

			ps.setString(1, comment.getText());
			ps.setInt(2, comment.getUserId());
			ps.setInt(3, comment.getMessageId());

			ps.executeUpdate();
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	public void delete(Connection connection, int commentId) throws SQLRuntimeException {
		PreparedStatement ps = null;
		String sql = "delete from comments where id = ?;";

		try {
			ps = connection.prepareStatement(sql);

			ps.setInt(1, commentId);

			ps.executeUpdate();
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}
}
