package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import beans.Message;
import exception.SQLRuntimeException;

public class MessageDao {

	public void insert(Connection connection, Message message) throws SQLRuntimeException {
		PreparedStatement ps = null;
		StringBuilder sql = new StringBuilder();

		sql.append("insert into messages ( ");
		sql.append("  title, ");
		sql.append("  text, ");
		sql.append("  category, ");
		sql.append("  user_id, ");
		sql.append("  created_date, ");
		sql.append("  updated_date ");
		sql.append(") values ( ");
		sql.append("  ?, ");
		sql.append("  ?, ");
		sql.append("  ?, ");
		sql.append("  ?, ");
		sql.append("  current_timestamp, ");
		sql.append("  current_timestamp);");

		try {
			ps = connection.prepareStatement(sql.toString());

			ps.setString(1, message.getTitle());
			ps.setString(2, message.getText());
			ps.setString(3, message.getCategory());
			ps.setInt(4, message.getUserId());

			ps.executeUpdate();
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	public void delete(Connection connection, int messageId) throws SQLRuntimeException {
		PreparedStatement ps = null;
		String sql = "delete from messages where id = ?;";

		try {
			ps = connection.prepareStatement(sql);

			ps.setInt(1, messageId);

			ps.executeUpdate();
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}
}
