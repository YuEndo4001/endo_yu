package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import beans.User;
import exception.NoRowsUpdatedRuntimeException;
import exception.SQLRuntimeException;

public class UserDao {

	public void insert(Connection connection, User user) throws SQLRuntimeException {
		PreparedStatement ps = null;
		StringBuilder sql = new StringBuilder();

		sql.append("insert into users ( ");
		sql.append("  account, ");
		sql.append("  password, ");
		sql.append("  name, ");
		sql.append("  branch_id, ");
		sql.append("  department_id ");
		sql.append(") values ( ");
		sql.append("  ?, ");
		sql.append("  ?, ");
		sql.append("  ?, ");
		sql.append("  ?, ");
		sql.append("  ? ");
		sql.append(");");

		try {
			ps = connection.prepareStatement(sql.toString());

			ps.setString(1, user.getAccount());
			ps.setString(2, user.getPassword());
			ps.setString(3, user.getName());
			ps.setInt(4, user.getBranchId());
			ps.setInt(5, user.getDepartmentId());

			ps.executeUpdate();
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	public User select(Connection connection, String account, String password)
			throws SQLRuntimeException, IllegalStateException {
		PreparedStatement ps = null;
		String sql = "select * from users where (account = ?) and (password = ?);";

		try {
			ps = connection.prepareStatement(sql);

			ps.setString(1, account);
			ps.setString(2, password);

			ResultSet rs = ps.executeQuery();
			List<User> users = toUsers(rs);

			if (users.isEmpty()) {
				return null;
			} else if (users.size() >= 2) {
				throw new IllegalStateException("ユーザーが重複しています。");
			}

			return users.get(0);
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	public User select(Connection connection, String account)
			throws SQLRuntimeException, IllegalStateException {
		PreparedStatement ps = null;
		StringBuilder sql = new StringBuilder();

		sql.append("select * from users where account = ?;");

		try {
			ps = connection.prepareStatement(sql.toString());

			ps.setString(1, account);

			ResultSet rs = ps.executeQuery();
			List<User> users = toUsers(rs);

			if (users.isEmpty()) {
				return null;
			} else if (users.size() >= 2) {
				throw new IllegalStateException("ユーザーが重複しています。");
			}

			return users.get(0);
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	public User select(Connection connection, int id) throws SQLRuntimeException, IllegalStateException {
		PreparedStatement ps = null;
		String sql = "select * from users where id = ?;";

		try {
			ps = connection.prepareStatement(sql);

			ps.setInt(1, id);

			ResultSet rs = ps.executeQuery();
			List<User> users = toUsers(rs);

			if (users.isEmpty()) {
				return null;
			} else if (users.size() >= 2) {
				throw new IllegalStateException("ユーザーが重複しています。");
			}

			return users.get(0);
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}

	}

	public void update(Connection connection, User user)
			throws SQLRuntimeException, NoRowsUpdatedRuntimeException {
		PreparedStatement ps = null;
		StringBuilder sql = new StringBuilder();

		sql.append("update users set ");
		sql.append("  account = ?, ");
		sql.append("  name = ?, ");
		sql.append("  branch_id = ?, ");
		sql.append("  department_id = ?, ");

		if (!StringUtils.isBlank(user.getPassword())) {
			sql.append("  password = ?, ");
		}

		sql.append("  updated_date = current_timestamp ");
		sql.append("where id = ?;");

		try {
			ps = connection.prepareStatement(sql.toString());

			ps.setString(1, user.getAccount());
			ps.setString(2, user.getName());
			ps.setInt(3, user.getBranchId());
			ps.setInt(4, user.getDepartmentId());

			if (!StringUtils.isBlank(user.getPassword())) {
				ps.setString(5, user.getPassword());
				ps.setInt(6, user.getId());
			} else {
				ps.setInt(5, user.getId());
			}

			int count = ps.executeUpdate();

			if (count == 0) {
				throw new NoRowsUpdatedRuntimeException();
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	public void update(Connection connection, int userId, int isStopped)
			throws SQLRuntimeException, NoRowsUpdatedRuntimeException {
		PreparedStatement ps = null;
		String sql = "update users set is_stopped = ? where id = ?;";

		try {
			ps = connection.prepareStatement(sql);

			ps.setInt(1, isStopped);
			ps.setInt(2, userId);

			int count = ps.executeUpdate();

			if (count == 0) {
				throw new NoRowsUpdatedRuntimeException();
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private List<User> toUsers(ResultSet rs) throws SQLRuntimeException {
		List<User> users = new ArrayList<>();

		try {
			while (rs.next()) {
				User user = new User();

				user.setId(rs.getInt("id"));
				user.setAccount(rs.getString("account"));
				user.setPassword(rs.getString("password"));
				user.setName(rs.getString("name"));
				user.setBranchId(rs.getInt("branch_id"));
				user.setDepartmentId(rs.getInt("department_id"));
				user.setIsStopped(rs.getByte("is_stopped"));
				user.setCreatedDate(rs.getDate("created_date"));
				user.setUpdatedDate(rs.getDate("updated_date"));

				users.add(user);
			}

			return users;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(rs);
		}
	}
}
