package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import beans.UserBranchDepartment;
import exception.SQLRuntimeException;

public class UserBranchDepartmentDao {

	public List<UserBranchDepartment> select(Connection connection) throws SQLRuntimeException {
		Statement statement = null;
		StringBuilder sql = new StringBuilder();

		sql.append("select ");
		sql.append("  users.id as id, ");
		sql.append("  users.account as account, ");
		sql.append("  users.name as user_name, ");
		sql.append("  branches.name as branch_name, ");
		sql.append("  departments.name as department_name, ");
		sql.append("  users.is_stopped as is_stopped ");
		sql.append("from users ");
		sql.append("inner join branches ");
		sql.append("  on users.branch_id = branches.id ");
		sql.append("inner join departments ");
		sql.append("  on users.department_id = departments.id ");
		sql.append("order by id desc;");

		try {
			statement = connection.createStatement();
			ResultSet rs = statement.executeQuery(sql.toString());
			List<UserBranchDepartment> users = toUsers(rs);

			return users;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(statement);
		}
	}

	private List<UserBranchDepartment> toUsers(ResultSet rs) throws SQLException {
		List<UserBranchDepartment> users = new ArrayList<>();

		try {
			while (rs.next()) {
				UserBranchDepartment user = new UserBranchDepartment();

				user.setId(rs.getInt("id"));
				user.setAccount(rs.getString("account"));
				user.setName(rs.getString("user_name"));
				user.setBranch(rs.getString("branch_name"));
				user.setDepartment(rs.getString("department_name"));
				user.setIsStopped(rs.getByte("is_stopped"));

				users.add(user);
			}

			return users;
		} finally {
			close(rs);
		}
	}
}
