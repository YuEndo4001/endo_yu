package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import beans.UserComment;
import exception.SQLRuntimeException;

public class UserCommentDao {

	public List<UserComment> select(Connection connection) throws SQLRuntimeException {
		PreparedStatement ps = null;
		StringBuilder sql = new StringBuilder();

		sql.append("select ");
		sql.append("  users.name as name, ");
		sql.append("  comments.text as text, ");
		sql.append("  users.id as user_id, ");
		sql.append("  comments.message_id as message_id, ");
		sql.append("  comments.id as comment_id, ");
		sql.append("  comments.created_date as created_date ");
		sql.append("from users ");
		sql.append("inner join comments ");
		sql.append("on users.id = comments.user_id ");
		sql.append("order by created_date asc;");

		try {
			ps = connection.prepareStatement(sql.toString());
			ResultSet rs = ps.executeQuery();
			List<UserComment> comments = toComments(rs);

			return comments;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private List<UserComment> toComments(ResultSet rs) throws SQLException {
		List<UserComment> comments = new ArrayList<>();

		try {
			while (rs.next()) {
				UserComment comment = new UserComment();

				comment.setName(rs.getString("name"));
				comment.setText(rs.getString("text"));
				comment.setUserId(rs.getInt("user_id"));
				comment.setMessageId(rs.getInt("message_id"));
				comment.setCommentId(rs.getInt("comment_id"));
				comment.setCreatedDate(rs.getTimestamp("created_date"));

				comments.add(comment);
			}

			return comments;
		} finally {
			close(rs);
		}
	}
}
