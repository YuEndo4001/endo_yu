package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import beans.Branch;
import exception.SQLRuntimeException;

public class BranchDao {

	public List<Branch> select(Connection connection) throws SQLRuntimeException {
		Statement statement = null;
		String sql = "select * from branches order by id asc";

		try {
			statement = connection.createStatement();
			ResultSet rs = statement.executeQuery(sql);
			List<Branch> branches = toBranches(rs);

			return branches;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(statement);
		}
	}

	private List<Branch> toBranches(ResultSet rs) throws SQLException {
		List<Branch> branches = new ArrayList<>();

		try {
			while (rs.next()) {
				Branch branch = new Branch();

				branch.setId(rs.getInt("id"));
				branch.setName(rs.getString("name"));
				branch.setCreatedDate(rs.getTimestamp("created_date"));
				branch.setUpdatedDate(rs.getTimestamp("updated_date"));

				branches.add(branch);
			}

			return branches;
		} finally {
			close(rs);
		}
	}
}
