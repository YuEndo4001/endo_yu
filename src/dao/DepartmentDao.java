package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import beans.Department;
import exception.SQLRuntimeException;

public class DepartmentDao {

	public List<Department> select(Connection connection) throws SQLRuntimeException {
		Statement statement = null;
		String sql = "select * from departments order by id asc;";

		try {
			statement = connection.createStatement();
			ResultSet rs = statement.executeQuery(sql);
			List<Department> departments = toDepartments(rs);

			return departments;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(statement);
		}
	}

	private List<Department> toDepartments(ResultSet rs) throws SQLException {
		List<Department> departments = new ArrayList<>();

		try {
			while (rs.next()) {
				Department department = new Department();

				department.setId(rs.getInt("id"));
				department.setName(rs.getString("name"));
				department.setCreatedDate(rs.getTimestamp("created_date"));
				department.setUpdatedDate(rs.getTimestamp("updated_date"));

				departments.add(department);
			}

			return departments;
		} finally {
			close(rs);
		}
	}
}
