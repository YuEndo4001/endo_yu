package filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebFilter( "/*" )
public class LoginFilter implements Filter {

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws ServletException, IOException {
		HttpServletRequest req = (HttpServletRequest) request;

		if (!req.getServletPath().equals("/login") && !req.getServletPath().equals("/css/main.css")) {
			HttpSession session = req.getSession();

			if (session.getAttribute("loginUser") == null) {
				List<String> errorMessages = new ArrayList<>();
				HttpServletResponse res = (HttpServletResponse) response;

				errorMessages.add("ログインしてください。");
				session.setAttribute("errorMessages", errorMessages);
				res.sendRedirect("./login");
				return;
			}
		}

		chain.doFilter(request, response);
	}

	@Override
	public void init(FilterConfig config) {
	}

	@Override
	public void destroy() {
	}
}
