package filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.User;


@WebFilter(urlPatterns = { "/management", "/setting", "/signup", "/stop" })
public class PrivilegeFilter implements Filter {

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws ServletException, IOException {
		HttpServletRequest req = (HttpServletRequest) request;
		HttpSession session = req.getSession();

		User user = (User) session.getAttribute("loginUser");
		int branchId = user.getBranchId();
		int departmentId = user.getDepartmentId();

		if ((branchId != 1) || (departmentId != 1)) {
			List<String> errorMessages = new ArrayList<>();
			HttpServletResponse res = (HttpServletResponse) response;

			errorMessages.add("このアカウントは権限を持っていません。");
			session.setAttribute("errorMessages", errorMessages);
			res.sendRedirect("./");
			return;
		}

		chain.doFilter(request, response);
	}

	@Override
	public void init(FilterConfig config) {
	}

	@Override
	public void destroy() {
	}
}
